﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfSumm
{
    class SumFunction
    {
        public void Sum(TextBox textBox2,Label otvet)
        {
            try
            {
                if (textBox2.Text == "")
                    return;
                var massiv = textBox2.Text.Split(',');
                var temp = 0;
                foreach (var d in massiv)
                {
                    temp += Convert.ToInt32(d);
                }
                otvet.Content = "Сумма: " + temp + "  Количество чисел: " + massiv.Length;
            }
            catch
            {
                otvet.Content = "Не верный формат массива";
            }
        }
    }
}
