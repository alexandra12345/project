﻿using System;

namespace WpfSumm
{
    internal class SumFunction
    {
        public string Sum(string textBox2)
        {
            try
            {
                if (textBox2 == "")
                    return "Не верный формат массива";
                var massiv = textBox2.Split(',');
                var temp = 0;
                foreach (var d in massiv)
                {
                    temp += Convert.ToInt32(d);
                }
                return "Сумма: " + temp + "  Количество чисел: " + massiv.Length;
            }
            catch
            {
                return "Не верный формат массива";
            }
        }
    }
}